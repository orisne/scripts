#!/usr/bin/env python3

# Main file for menu navigations

from simple_term_menu import TerminalMenu

def main():
    main_menu = [
        "[0] Hardware tests",
        "[1] Nozzle control",
        "[2] Test"
        ]
    terminal_menu = TerminalMenu(main_menu, title="\nOptions:\n")
    menu_entry_index = terminal_menu.show()
    print(f"You have selected {main_menu[menu_entry_index]}!")

if __name__ == "__main__":
    main()
    