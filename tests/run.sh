docker run \
    --rm \
    --privileged \
    -it \
    --name hwtests \
    -v $(pwd):/app/src \
    --entrypoint=python \
    hwtests "/app/src/main.py"